class Player(object):

    def __init__(self):
        pass

class Board(object):

    def __init__(self):
        pass

    def is_completed(self, player):
        return False

    def best_move(self, player):
        

class Game(object):

    def __init__(self, board, players, depth=4):
        self.board = board
        self.players = players
        self.depth = depth

    def start(self):
        while True:
            for player in self.players:
                print('Turns to {}'.format(player))
                if self.board.is_completed(player):
                    print('Player {} cannot play anymore'.format(player))
                    exit(0)
                